package main

import "fmt"

type Human struct {
	name string
}

func (h *Human) getName() string {
	return h.name
}

func (h *Human) setName(n string) {
	h.name = n
}

type Action struct {
	Human
	time uint64
}

func (a *Action) getTime() uint64 {
	return a.time
}

func (a *Action) setTime(t uint64) {
	a.time = t
}

func main() {
	a := Action{}
	a.setName("Peter")
	a.setTime(150)
	fmt.Printf("%s is busy for %d seconds\n", a.getName(), a.getTime())
}
