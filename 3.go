package main

import (
	"fmt"
	"sync"
)

func main() {
	a := [5]int64{2, 4, 6, 8, 10}
	var s [5]int64
	wg := sync.WaitGroup{}

	wg.Add(len(a))
	for i, x := range a {
		go func(i int, x int64) {
			s[i] = x * x
			wg.Done()
		}(i, x)
	}

	wg.Wait()

	var sum int64
	for _, x := range s {
		sum += x
	}
	fmt.Println(sum)
}
