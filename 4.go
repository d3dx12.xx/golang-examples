package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"syscall"
)

func main() {
	ch := make(chan interface{})
	wg := sync.WaitGroup{}

	n_str := "4"
	if len(os.Args) > 1 {
		n_str = os.Args[1]
	}
	n, err := strconv.ParseInt(n_str, 10, 32)
	if err != nil {
		fmt.Println("failed to parse thread quantity:", err)
		os.Exit(1)
	}
	wg.Add(int(n))
	for i := 0; i < int(n); i++ {
		go func(i int, ch chan interface{}) {
			for v, ok := <-ch; ok; v, ok = <-ch {
				fmt.Println("worker", i, "value", v)
			}
			fmt.Println(i, "exiting")
			wg.Done()
		}(i, ch)
	}

	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	quit := false
	go func() {
		<-sig
		quit = true
	}()

	for !quit {
		ch <- rand.Int31()
	}
	close(ch)

	wg.Wait()
}
