package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {
	ch := make(chan interface{})

	n_str := "5"
	if len(os.Args) > 1 {
		n_str = os.Args[1]
	}
	n, err := strconv.ParseInt(n_str, 10, 64)
	if err != nil {
		fmt.Println("failed to parse thread quantity:", err)
		os.Exit(1)
	}
	t := time.NewTimer(time.Second * time.Duration(n))

	quit := false
	go func() {
		<-t.C
		quit = true
	}()

	go func(ch chan interface{}) {
		for !quit {
			ch <- rand.Int31()
			time.Sleep(time.Millisecond * 250)
		}
		close(ch)
		fmt.Println("tx exiting")
	}(ch)

	for v, ok := <-ch; ok; v, ok = <-ch {
		fmt.Println("received", v)
	}
	fmt.Println("rx exiting")
}
