package main

import (
	"fmt"
	"sync"
)

func main() {
	wg := sync.WaitGroup{}
	mu := sync.Mutex{}
	m := map[int]int{}

	threads := 6
	wg.Add(threads)
	for n := 0; n < threads; n++ {
		go func(n int) {
			mu.Lock()
			m[n] = 0
			mu.Unlock()
			defer wg.Done()
			for i := n * 20; i < (n+1)*20; i++ {
				mu.Lock()
				m[n] += i * i
				mu.Unlock()
			}
		}(n)
	}
	wg.Wait()

	s := 0
	fmt.Println(m)
	for _, v := range m {
		s += v
	}
	fmt.Println(s)
}
